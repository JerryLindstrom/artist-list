package Artists;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.HashSet;
import java.util.List;

public class ArtistManager {
    private final String filePath;

    public ArtistManager(String filePath) {
        this.filePath = filePath;
    }

    public void printArtists() {
        try (FileReader fileReader = new FileReader(filePath);
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            System.out.println("Hello there.. here is your list of great artists/bands you have previously entered:");
            String line;
            while ((line = bufferedReader.readLine()) !=null) {
                HashSet<String> textList = new HashSet<>();  //make an hashset with entries from .txt file
                textList.add(line);                         //trying to make a list to compare input vs list..
                System.out.println(line);
            }
        }   catch (Exception e) {
                System.out.println("Artist database is currently empty..");
                System.out.println("I will create a new file for you.");
            }
    }

    public void writeArtists(List<String> artists) {
        try (FileWriter fileWriter = new FileWriter(filePath, true);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {
                for (String artist : artists)

                    bufferedWriter.write(artist + "\n");
            }

         catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }




}
