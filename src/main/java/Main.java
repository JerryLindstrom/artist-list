import Artists.ArtistManager;

import java.io.File;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        ArtistManager artistManager = new ArtistManager("src/artists.txt");
        artistManager.printArtists();

        List<String> artists = new ArrayList<>();

        Scanner input = new Scanner(System.in);

        while (true) {
            System.out.println("---------------------------------------------");
            System.out.print("Type in some good artists/bands you like to listen to or," + "\n" +
                    "type 'display' to show newly entered artist/band," + "\n" +
                    "type 'clear' to empty the newly inputted list" + "\n" +
                    "type 'exit' to save your input and exit the program.. : ");
            String userInput = input.nextLine();

            artists.add(userInput);

            if (userInput.equals("clear")) {
                artists.clear();
                System.out.println("Your latest entries are gone now.." + "\n");
            }

            if(userInput.equals("display")) {
                artists.remove("display");
                System.out.println("Here are your most recent entries: " + artists + "\n");
            }
            if(userInput.equals("fileinfo")) {
                String fileName = "src/artists.txt";
                File f = new File(fileName);
                long fileSize = f.length();
                System.out.format("The size of the file: %d bytes", fileSize);
                artists.remove("fileinfo");
                System.out.println("\n");
            }

            if (userInput.equals("exit")) {
                artists.remove("exit");
                artistManager.writeArtists(artists);
                break;
            }
        }
    }
}
